package dhsavell.sam.responder

import io.kotlintest.shouldBe
import io.kotlintest.specs.WordSpec

class PositionalPatternSpec : WordSpec({
    "A 'Beginning' pattern" should {
        "match a single word at the beginning of a string" {
            Beginning.containsMatch("this is a test", "this") shouldBe true
            Beginning.containsMatch("this is a test", "is") shouldBe false
            Beginning.containsMatch("this is a test", "a") shouldBe false
            Beginning.containsMatch("this is a test", "test") shouldBe false
        }

        "match a phrase at the beginning of a string" {
            Beginning.containsMatch("this is a test", "this is") shouldBe true
        }

        "match a word when it is equal to the given string" {
            Beginning.containsMatch("hello", "hello") shouldBe true
            Beginning.containsMatch("hello, world!", "hello, world!") shouldBe true
        }

        "not match a string if it isn't a standalone word" {
            Beginning.containsMatch("firetruck", "fire") shouldBe false
            Beginning.containsMatch("hello world", "hello w") shouldBe false
        }

        "not match a word not contained in the string" {
            Beginning.containsMatch("this is a test", "hello") shouldBe false
        }
    }

    "An 'Anywhere' pattern" should {
        "match a word or phrase anywhere in a string" {
            Anywhere.containsMatch("this is a test", "this") shouldBe true
            Anywhere.containsMatch("this is a test", "is") shouldBe true
            Anywhere.containsMatch("this is a test", "a") shouldBe true
            Anywhere.containsMatch("this is a test", "test") shouldBe true
            Anywhere.containsMatch("this is a test", "is a") shouldBe true
            Anywhere.containsMatch("this is a test", "a test") shouldBe true
        }

        "match a word when it is equal to the given string" {
            Anywhere.containsMatch("hello", "hello") shouldBe true
            Anywhere.containsMatch("hello, world!", "hello, world!") shouldBe true
        }

        "not match parts of individual words" {
            Anywhere.containsMatch("firetruck", "fire") shouldBe false
            Anywhere.containsMatch("hello world", "hello w") shouldBe false
        }

        "not match a word not contained in the string" {
            Anywhere.containsMatch("this is a test", "hello") shouldBe false
        }
    }

    "An 'End' pattern" should {
        "match a word or phrase anywhere in a string" {
            End.containsMatch("this is a test", "this") shouldBe false
            End.containsMatch("this is a test", "is") shouldBe false
            End.containsMatch("this is a test", "a") shouldBe false
            End.containsMatch("this is a test", "test") shouldBe true
            End.containsMatch("this is a test", "is a") shouldBe false
            End.containsMatch("this is a test", "a test") shouldBe true
        }

        "match a word when it is equal to the given string" {
            End.containsMatch("hello", "hello") shouldBe true
            End.containsMatch("hello, world!", "hello, world!") shouldBe true
        }

        "not match parts of individual words" {
            End.containsMatch("firetruck", "truck") shouldBe false
            End.containsMatch("hello world", "lo world") shouldBe false
        }

        "not match a word not contained in the string" {
            End.containsMatch("this is a test", "hello") shouldBe false
        }
    }
})