package dhsavell.sam

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.types.path
import de.swirtz.ktsrunner.objectloader.KtsObjectLoader
import dhsavell.sam.responder.Responder
import mu.KotlinLogging
import org.jetbrains.kotlin.cli.common.environment.setIdeaIoUseFallback
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.stream.Collectors

class SamLauncher : CliktCommand() {
    private val token by option(help = "discord bot token", envvar = "TOKEN").required()
    private val scriptDir by option(help = "location of scripts", envvar = "SCRIPT_DIR")
        .path()
        .default(Paths.get("./scripts"))

    private val bootstrapLogger = KotlinLogging.logger { }

    private fun loadResponders(scriptDir: Path): List<Responder> {
        setIdeaIoUseFallback()

        return Files.walk(scriptDir)
            .filter { path -> Files.isRegularFile(path) && path.toString().endsWith(".kts") }
            .peek { path -> bootstrapLogger.info { "Loading responder script: $path" } }
            .map { path -> KtsObjectLoader().load<Responder>(Files.newBufferedReader(path)) }
            .collect(Collectors.toList())
    }

    override fun run() {
        SamBot(
            token = token,
            responders = loadResponders(scriptDir)
        ).runForever()
    }
}

fun main(args: Array<String>) = SamLauncher().main(args)