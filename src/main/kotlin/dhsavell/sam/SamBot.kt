package dhsavell.sam

import dhsavell.sam.responder.Responder
import mu.KotlinLogging
import sx.blah.discord.api.ClientBuilder
import sx.blah.discord.api.IDiscordClient
import sx.blah.discord.api.events.EventSubscriber
import sx.blah.discord.handle.impl.events.ReadyEvent
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent

/**
 * Primary bot class, used to wire up a Discord client to a list of Responders.
 */
@Suppress("UNUSED_PARAMETER", "unused")
class SamBot(
    private val token: String,
    private val client: IDiscordClient = ClientBuilder().withToken(token).build(),
    private val responders: List<Responder>
) {

    init {
        client.dispatcher.registerListener(this)
    }

    private val logger = KotlinLogging.logger { }

    /**
     * Runs the bot indefinitely, blocking the main thread.
     */
    fun runForever() = client.login()

    @EventSubscriber
    fun handleBotReady(event: ReadyEvent) {
        logger.info { "Sam the Minuteman lives" }
        logger.info { "Logged in as ${client.ourUser.name}#${client.ourUser.discriminator} (${client.ourUser.longID})" }
    }

    @EventSubscriber
    fun handleMessageReceived(event: MessageReceivedEvent) {
        val message = event.message

        if (message.author != client.ourUser) {
            responders
                .filter { responder -> responder.triggers.any { trigger -> trigger(message) } }
                .forEach { responder -> responder.respond(this, message) }
        }
    }
}