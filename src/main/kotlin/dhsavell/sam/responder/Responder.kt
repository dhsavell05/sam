package dhsavell.sam.responder

import dhsavell.sam.SamBot
import sx.blah.discord.handle.obj.IMessage

typealias ResponseInvoker = (IMessage) -> Boolean

/**
 * Represents a piece of bot functionality that responds to messages meeting certain conditions
 */
interface Responder {
    val name: String
    val triggers: List<ResponseInvoker>
    fun respond(bot: SamBot, context: IMessage)
}