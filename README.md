# sam

> The pinnacle of Discord bot technology, bringing you more of Sam the Minuteman
> than you could ever possibly want.

Running the bot requires a token from Discord and a path to a directory of responder
scripts.

## Writing Responders

### Scripting

Responder scripts are written in Kotlin, and can be dynamically be loaded from
any directory. If using IntelliJ IDEA, the `scripts/` directory will already
be set to use the appropriate classpath and autocomplete will be available.

### Responder DSL

A responder definition will look something like this:

```kotlin
newResponder {
    name { "A Cool Responder" }
    
    respondsTo {
        // Matchers for phrases at the beginning, the end, or anywhere in a
        // message are available.
        "Some word or phrase 1" at Beginning
        "Some word or phrase 2" at Anywhere
        "Some word or phrase 3" at End
        
        // Custom predicates can also be used.
        condition { message ->
            message.content.length % 2 == 0
        }
    }
    
    // Only one of the following 2 blocks should be present! If multiple actions
    // are present, the last one will be used.
    
    // For basic responses that send a message:
    answersWith {
        "A closure that returns some string that will be sent as a message"
    }
    
    // For more advanced actions:
    whenActivated { bot, message ->
        println("Some kind of other action, with no need to return a String")
        message.channel.sendMessage("Messages can still be sent from here!")
    }
}
```

Responder definitions are plain Kotlin code, so additional logic is possible.